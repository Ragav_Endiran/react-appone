import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';


// this section is used to render the app into DOM based on ID
window.renderAppOne = containerId => {
  ReactDOM.render(<App />, document.getElementById(containerId));
}

// this section is used to remove the app from DOM based on ID
window.unmountAppOne = containerId => {
  ReactDOM.unmountComponentAtNode(document.getElementById(containerId));
}

// this section is used to run the applicatio independently
if(!document.getElementById("AppOne-container")) {
  ReactDOM.render(<App />, document.getElementById("root"));
}